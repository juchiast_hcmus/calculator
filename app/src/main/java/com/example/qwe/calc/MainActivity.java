package com.example.qwe.calc;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ValueCallback<String> {
    private String text = "";
    private WebView webView = null;
    private int default_color;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (webView == null) {
            webView = new WebView(this);
            webView.getSettings().setJavaScriptEnabled(true);
        }
        addGridButtons();
        displayText();
        default_color = ((TextView)findViewById(R.id.view)).getTextColors().getDefaultColor();
    }

    private void displayText() {
        ((TextView) findViewById(R.id.view)).setText(text);
        setTextSize();
    }

    private void addGridButtons() {
        String[] labels = {
                "C", "⌫",
                "7", "8", "9", "+",
                "4", "5", "6", "-",
                "1", "2", "3", "×",
                "0", ".", "=", "÷"
        };
        GridLayout grid = findViewById(R.id.grid);
        for (String label : labels) {
            Button button = new Button(this);
            button.setText(label);
            button.setOnClickListener(this);
            if (label.equals("C") || label.equals("⌫")) {
                GridLayout.LayoutParams params = (GridLayout.LayoutParams) button.getLayoutParams();
                if (params == null) {
                    params = new GridLayout.LayoutParams();
                }
                params.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 2);
                button.setLayoutParams(params);
            }
            grid.addView(button);
        }
    }

    private void setTextSize() {
        float screen_height = getScreenHeightPx();
        float size = screen_height / 10;

        float screen_width = getScreenWidthPx() * 0.9f;
        float text_width = getTextWidth(size);
        if (text_width >= screen_width) {
            size = size * (screen_width / text_width);
        }
        ((TextView)findViewById(R.id.view)).setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }

    private void calculate(String expr) {
        webView.evaluateJavascript("(function(){return " + expr + ";})()", this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setTextSize();
    }


    @Override
    public void onClick(View v) {
        String label = ((Button) v).getText().toString();
        if (label.equals("C")) {
            text = "";
            displayText();
        } else if (label.equals("⌫")) {
            if (text.length() > 0) {
                text = text.substring(0, text.length() - 1);
                displayText();
            }
        } else if (label.equals("=")) {
            String expr = text.replace('÷', '/').replace('×', '*');
            calculate(expr);
        } else {
            text += label;
            displayText();
        }
    }

    @Override
    public void onReceiveValue(String value) {
        TextView view = findViewById(R.id.view);
        if (!value.equals("null")) {
            text = value;
            displayText();
            view.setTextColor(default_color);
        } else {
            view.setTextColor(Color.RED);
        }
    }

    public float getScreenHeightPx() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public float getScreenWidthPx() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    private int getTextWidth(float text_size) {
        Paint paint = new Paint();

        TextView view = findViewById(R.id.view);

        paint.setTypeface(view.getTypeface());

        float scaledSizeInPixels = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                text_size,
                this.getResources().getDisplayMetrics());
        paint.setTextSize(scaledSizeInPixels);

        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }
}